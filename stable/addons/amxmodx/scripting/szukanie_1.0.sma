#include <amxmodx>
#include <amxmisc>
#include <sqlx>
#include <colorchat>

#define PLUGIN "Szukanie aktywnych Banow"
#define VERSION "1.0"
#define AUTHOR "xenos"
#define czas 1
#define flaga ADMIN_BAN
new Handle:Tuple;
new cvar_host,cvar_user,cvar_password,cvar_db;
new bannuj_id[33];
new idadmina,iddozbanowania;
new  szName[32]
public plugin_init() {
        register_plugin(PLUGIN, VERSION, AUTHOR)
        register_concmd("amx_szukaj","Szukaj",flaga, "- Szukanie aktywnych banow");
        cvar_host= register_cvar("amx_sql_host", "127.0.0.1");  
        cvar_user = register_cvar("amx_sql_user", "root"); 
        cvar_password = register_cvar("amx_sql_pass", ""); 
        cvar_db = register_cvar("amx_sql_db", "amxbans");
	
	
	
        new szHost[64], szUser[32], szPass[32], szDB[128];
        get_pcvar_string( cvar_host, szHost, charsmax( szHost ) );
        get_pcvar_string( cvar_user, szUser, charsmax( szUser ) );
        get_pcvar_string( cvar_password, szPass, charsmax( szPass ) );
        get_pcvar_string(cvar_db, szDB, charsmax( szDB ) );
        Tuple = SQL_MakeDbTuple( szHost, szUser, szPass, szDB );
}


public make(id)
{
new Data[1]
Data[0] = id

new qCommand[512]
get_user_name(id, szName, charsmax(szName))
ColorChat(id,RED,"nick %s",szName)
format(qCommand, sizeof qCommand-1, "SELECT * FROM  `amx_bans` WHERE  `player_nick` LIKE '%s' AND `ban_reason` NOT LIKE 'podszywka' AND `expired` NOT LIKE 1", szName)
SQL_ThreadQuery(Tuple, "read", qCommand , Data, 1)

}


public read(FailState, Handle:Query, Errorcode, Error[], Data[], DataSize)
{

        
        
        if(FailState == TQUERY_CONNECT_FAILED)
        {
                log_amx("Nie mozna podlaczyc sie do bazy danych.")
                return PLUGIN_CONTINUE
        }
        
        
        if(!SQL_MoreResults(Query)) 
	ColorChat(idadmina,RED,"[Szukanie Banow] Nie znalazlem gracza %s w bazie",szName);
        else 
        {
		
                menuban(idadmina,iddozbanowania)
            
        }
        
        return PLUGIN_CONTINUE
}


public Szukaj(id)
{
	if( !(get_user_flags(id) & flaga) )
	{
		
	ColorChat(id,RED,"[Szukanie Banow] Brak uprawnien");
	return PLUGIN_HANDLED;
	}

	idadmina=id;
	new menu = menu_create("\ySprawdz Gracza:", "Szukaj_Handle");
	new cb = menu_makecallback("Szukaj_Callback");
	for(new i=0, n=0; i<=32; i++)
	{
		if(!is_user_connected(i))
			continue;
		bannuj_id[n++] = i;
		new nazwa_gracza[64];
		get_user_name(i, nazwa_gracza, 63)
		menu_additem(menu, nazwa_gracza, "0", 0, cb);
	}
	menu_display(id, menu);

	return PLUGIN_HANDLED;
}



public Szukaj_Handle(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		menu_destroy(menu);
		return PLUGIN_CONTINUE;
	}
	
	if(!is_user_connected(bannuj_id[item]))
	{
		ColorChat(id, GREEN, "[Sprawdzenie]^x01 Nie odnaleziono danego gracza.");
		return PLUGIN_CONTINUE;
	}
	iddozbanowania=bannuj_id[item];
	make(bannuj_id[item]);
	
	
	
	return PLUGIN_CONTINUE;
}

public Szukaj_Callback(id, menu, item)
{
	if(bannuj_id[item] == id || bannuj_id[item]==(is_user_hltv(item)) || ( (get_user_flags(item) & flaga) ) || ( (get_user_flags(item) & ADMIN_IMMUNITY) )) 
		return ITEM_DISABLED;
	return ITEM_ENABLED;
}


public menuban(idadmina,iddozbanowania)
{	
new gMyMenu;	
	
new msg[128];
format(msg,127,"Znaleziono %s zbanowac?\n na czas %d minut(0 min=perm)",szName,czas);	
gMyMenu=menu_create(msg,"cbMyMenu");
menu_additem(gMyMenu,"Zbanowac");
menu_additem(gMyMenu,"Nie banujemy");
menu_display(idadmina, gMyMenu,0)	
}

public cbMyMenu(id, menu, item){
	switch(item){
		case 0:{
			client_cmd(id, "amx_ban %d %s Aktywny-Ban",czas,szName);
		}
		case 1:{
			szName="";
			
		}
		
	}
}
/* AMXX-Studio Notes - DO NOT MODIFY BELOW HERE
*{\\ rtf1\\ ansi\\ ansicpg1250\\ uc1\\ deff0\\ deflang1045\\ deflangfe1045{\\ fonttbl{\\ f0 Tahoma;}}\n\\ f0{\\ colortbl;}{\\ *\\ generator Wine Riched20 2.0.????;}\\ pard\\ sl-240\\ slmult1\\ li0\\ fi0\\ ri0\\ sa0\\ sb0\\ s-1\\ cfpat0\\ cbpat0\n\\ par}
*/
